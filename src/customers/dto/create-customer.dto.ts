import { IsNotEmpty } from 'class-validator';
import { Length } from 'class-validator/types/decorator/decorators';

export class CreateCustomerDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  age: number;

  @IsNotEmpty()
  @Length(10)
  tel: string;

  @IsNotEmpty()
  gender: string;
}
